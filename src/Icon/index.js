import { academicCap } from "./icons/academic-cap";
import { adjustments } from "./icons/adjustments";
import { annotation } from "./icons/annotation";
import { archive } from "./icons/archive";
import { arrowCircleDown } from "./icons/arrow-circle-down";
import { arrowCircleLeft } from "./icons/arrow-circle-left";
import { arrowCircleRight } from "./icons/arrow-circle-right";
import { arrowCircleUp } from "./icons/arrow-circle-up";
import { arrowDown } from "./icons/arrow-down";
import { arrowLeft } from "./icons/arrow-left";
import { arrowNarrowDown } from "./icons/arrow-narrow-down";
import { arrowNarrowLeft } from "./icons/arrow-narrow-left";
import { arrowNarrowRight } from "./icons/arrow-narrow-right";
import { arrowNarrowUp } from "./icons/arrow-narrow-up";
import { arrowRight } from "./icons/arrow-right";
import { arrowUp } from "./icons/arrow-up";
import { arrowsExpand } from "./icons/arrows-expand";
import { atSymbol } from "./icons/at-symbol";
import { backspace } from "./icons/backspace";
import { badgeCheck } from "./icons/badge-check";
import { ban } from "./icons/ban";
import { beaker } from "./icons/beaker";
import { bell } from "./icons/bell";
import { bookOpen } from "./icons/book-open";
import { bookmarkAlt } from "./icons/bookmark-alt";
import { bookmark } from "./icons/bookmark";
import { briefcase } from "./icons/briefcase";
import { cake } from "./icons/cake";
import { calculator } from "./icons/calculator";
import { calendar } from "./icons/calendar";
import { camera } from "./icons/camera";
import { cash } from "./icons/cash";
import { chartBar } from "./icons/chart-bar";
import { chartPie } from "./icons/chart-pie";
import { chartSquareBar } from "./icons/chart-square-bar";
import { chatAlt2 } from "./icons/chat-alt-2";
import { chatAlt } from "./icons/chat-alt";
import { chat } from "./icons/chat";
import { checkCircle } from "./icons/check-circle";
import { check } from "./icons/check";
import { chevronDoubleDown } from "./icons/chevron-double-down";
import { chevronDoubleLeft } from "./icons/chevron-double-left";
import { chevronDoubleRight } from "./icons/chevron-double-right";
import { chevronDoubleUp } from "./icons/chevron-double-up";
import { chevronDown } from "./icons/chevron-down";
import { chevronLeft } from "./icons/chevron-left";
import { chevronRight } from "./icons/chevron-right";
import { chevronUp } from "./icons/chevron-up";
import { chip } from "./icons/chip";
import { clipboardCheck } from "./icons/clipboard-check";
import { clipboardCopy } from "./icons/clipboard-copy";
import { clipboardList } from "./icons/clipboard-list";
import { clipboard } from "./icons/clipboard";
import { clock } from "./icons/clock";
import { cloudDownload } from "./icons/cloud-download";
import { cloudUpload } from "./icons/cloud-upload";
import { cloud } from "./icons/cloud";
import { code } from "./icons/code";
import { cog } from "./icons/cog";
import { collection } from "./icons/collection";
import { colorSwatch } from "./icons/color-swatch";
import { creditCard } from "./icons/credit-card";
import { cubeTransparent } from "./icons/cube-transparent";
import { cube } from "./icons/cube";
import { currencyBangladeshi } from "./icons/currency-bangladeshi";
import { currencyDollar } from "./icons/currency-dollar";
import { currencyEuro } from "./icons/currency-euro";
import { currencyPound } from "./icons/currency-pound";
import { currencyRupee } from "./icons/currency-rupee";
import { currencyYen } from "./icons/currency-yen";
import { cursorClick } from "./icons/cursor-click";
import { database } from "./icons/database";
import { desktopComputer } from "./icons/desktop-computer";
import { deviceMobile } from "./icons/device-mobile";
import { deviceTablet } from "./icons/device-tablet";
import { documentAdd } from "./icons/document-add";
import { documentDownload } from "./icons/document-download";
import { documentDuplicate } from "./icons/document-duplicate";
import { documentRemove } from "./icons/document-remove";
import { documentReport } from "./icons/document-report";
import { documentSearch } from "./icons/document-search";
import { documentText } from "./icons/document-text";
import { document } from "./icons/document";
import { dotsCircleHorizontal } from "./icons/dots-circle-horizontal";
import { dotsHorizontal } from "./icons/dots-horizontal";
import { dotsVertical } from "./icons/dots-vertical";
import { download } from "./icons/download";
import { duplicate } from "./icons/duplicate";
import { emojiHappy } from "./icons/emoji-happy";
import { emojiSad } from "./icons/emoji-sad";
import { exclamationCircle } from "./icons/exclamation-circle";
import { exclamation } from "./icons/exclamation";
import { externalLink } from "./icons/external-link";
import { eyeOff } from "./icons/eye-off";
import { eye } from "./icons/eye";
import { fastForward } from "./icons/fast-forward";
import { film } from "./icons/film";
import { filter } from "./icons/filter";
import { fingerPrint } from "./icons/finger-print";
import { fire } from "./icons/fire";
import { flag } from "./icons/flag";
import { folderAdd } from "./icons/folder-add";
import { folderDownload } from "./icons/folder-download";
import { folderOpen } from "./icons/folder-open";
import { folderRemove } from "./icons/folder-remove";
import { folder } from "./icons/folder";
import { gift } from "./icons/gift";
import { globeAlt } from "./icons/globe-alt";
import { globe } from "./icons/globe";
import { hand } from "./icons/hand";
import { hashtag } from "./icons/hashtag";
import { heart } from "./icons/heart";
import { home } from "./icons/home";
import { identification } from "./icons/identification";
import { inboxIn } from "./icons/inbox-in";
import { inbox } from "./icons/inbox";
import { informationCircle } from "./icons/information-circle";
import { key } from "./icons/key";
import { library } from "./icons/library";
import { lightBulb } from "./icons/light-bulb";
import { lightningBolt } from "./icons/lightning-bolt";
import { link } from "./icons/link";
import { locationMarker } from "./icons/location-marker";
import { lockClosed } from "./icons/lock-closed";
import { lockOpen } from "./icons/lock-open";
import { login } from "./icons/login";
import { logout } from "./icons/logout";
import { mailOpen } from "./icons/mail-open";
import { mail } from "./icons/mail";
import { map } from "./icons/map";
import { menuAlt1 } from "./icons/menu-alt-1";
import { menuAlt2 } from "./icons/menu-alt-2";
import { menuAlt3 } from "./icons/menu-alt-3";
import { menuAlt4 } from "./icons/menu-alt-4";
import { menu } from "./icons/menu";
import { microphone } from "./icons/microphone";
import { minusCircle } from "./icons/minus-circle";
import { minusSm } from "./icons/minus-sm";
import { minus } from "./icons/minus";
import { moon } from "./icons/moon";
import { musicNote } from "./icons/music-note";
import { newspaper } from "./icons/newspaper";
import { officeBuilding } from "./icons/office-building";
import { paperAirplane } from "./icons/paper-airplane";
import { paperClip } from "./icons/paper-clip";
import { pause } from "./icons/pause";
import { pencilAlt } from "./icons/pencil-alt";
import { pencil } from "./icons/pencil";
import { phoneIncoming } from "./icons/phone-incoming";
import { phoneMissedCall } from "./icons/phone-missed-call";
import { phoneOutgoing } from "./icons/phone-outgoing";
import { phone } from "./icons/phone";
import { photograph } from "./icons/photograph";
import { play } from "./icons/play";
import { plusCircle } from "./icons/plus-circle";
import { plusSm } from "./icons/plus-sm";
import { plus } from "./icons/plus";
import { presentationChartBar } from "./icons/presentation-chart-bar";
import { presentationChartLine } from "./icons/presentation-chart-line";
import { printer } from "./icons/printer";
import { puzzle } from "./icons/puzzle";
import { qrcode } from "./icons/qrcode";
import { questionMarkCircle } from "./icons/question-mark-circle";
import { receiptRefund } from "./icons/receipt-refund";
import { receiptTax } from "./icons/receipt-tax";
import { refresh } from "./icons/refresh";
import { reply } from "./icons/reply";
import { rewind } from "./icons/rewind";
import { rss } from "./icons/rss";
import { saveAs } from "./icons/save-as";
import { save } from "./icons/save";
import { scale } from "./icons/scale";
import { scissors } from "./icons/scissors";
import { searchCircle } from "./icons/search-circle";
import { search } from "./icons/search";
import { selector } from "./icons/selector";
import { server } from "./icons/server";
import { share } from "./icons/share";
import { shieldCheck } from "./icons/shield-check";
import { shieldExclamation } from "./icons/shield-exclamation";
import { shoppingBag } from "./icons/shopping-bag";
import { shoppingCart } from "./icons/shopping-cart";
import { sortAscending } from "./icons/sort-ascending";
import { sortDescending } from "./icons/sort-descending";
import { sparkles } from "./icons/sparkles";
import { speakerphone } from "./icons/speakerphone";
import { star } from "./icons/star";
import { statusOffline } from "./icons/status-offline";
import { statusOnline } from "./icons/status-online";
import { stop } from "./icons/stop";
import { sun } from "./icons/sun";
import { support } from "./icons/support";
import { switchHorizontal } from "./icons/switch-horizontal";
import { switchVertical } from "./icons/switch-vertical";
import { table } from "./icons/table";
import { tag } from "./icons/tag";
import { template } from "./icons/template";
import { terminal } from "./icons/terminal";
import { thumbDown } from "./icons/thumb-down";
import { thumbUp } from "./icons/thumb-up";
import { ticket } from "./icons/ticket";
import { translate } from "./icons/translate";
import { trash } from "./icons/trash";
import { trendingDown } from "./icons/trending-down";
import { trendingUp } from "./icons/trending-up";
import { truck } from "./icons/truck";
import { upload } from "./icons/upload";
import { userAdd } from "./icons/user-add";
import { userCircle } from "./icons/user-circle";
import { userGroup } from "./icons/user-group";
import { userRemove } from "./icons/user-remove";
import { user } from "./icons/user";
import { users } from "./icons/users";
import { variable } from "./icons/variable";
import { videoCamera } from "./icons/video-camera";
import { viewBoards } from "./icons/view-boards";
import { viewGridAdd } from "./icons/view-grid-add";
import { viewGrid } from "./icons/view-grid";
import { viewList } from "./icons/view-list";
import { volumeOff } from "./icons/volume-off";
import { volumeUp } from "./icons/volume-up";
import { wifi } from "./icons/wifi";
import { xCircle } from "./icons/x-circle";
import { x } from "./icons/x";
import { zoomIn } from "./icons/zoom-in";
import { zoomOut } from "./icons/zoom-out";
export {
    academicCap,
    adjustments,
    annotation,
    archive,
    arrowCircleDown,
    arrowCircleLeft,
    arrowCircleRight,
    arrowCircleUp,
    arrowDown,
    arrowLeft,
    arrowNarrowDown,
    arrowNarrowLeft,
    arrowNarrowRight,
    arrowNarrowUp,
    arrowRight,
    arrowUp,
    arrowsExpand,
    atSymbol,
    backspace,
    badgeCheck,
    ban,
    beaker,
    bell,
    bookOpen,
    bookmarkAlt,
    bookmark,
    briefcase,
    cake,
    calculator,
    calendar,
    camera,
    cash,
    chartBar,
    chartPie,
    chartSquareBar,
    chatAlt2,
    chatAlt,
    chat,
    checkCircle,
    check,
    chevronDoubleDown,
    chevronDoubleLeft,
    chevronDoubleRight,
    chevronDoubleUp,
    chevronDown,
    chevronLeft,
    chevronRight,
    chevronUp,
    chip,
    clipboardCheck,
    clipboardCopy,
    clipboardList,
    clipboard,
    clock,
    cloudDownload,
    cloudUpload,
    cloud,
    code,
    cog,
    collection,
    colorSwatch,
    creditCard,
    cubeTransparent,
    cube,
    currencyBangladeshi,
    currencyDollar,
    currencyEuro,
    currencyPound,
    currencyRupee,
    currencyYen,
    cursorClick,
    database,
    desktopComputer,
    deviceMobile,
    deviceTablet,
    documentAdd,
    documentDownload,
    documentDuplicate,
    documentRemove,
    documentReport,
    documentSearch,
    documentText,
    document,
    dotsCircleHorizontal,
    dotsHorizontal,
    dotsVertical,
    download,
    duplicate,
    emojiHappy,
    emojiSad,
    exclamationCircle,
    exclamation,
    externalLink,
    eyeOff,
    eye,
    fastForward,
    film,
    filter,
    fingerPrint,
    fire,
    flag,
    folderAdd,
    folderDownload,
    folderOpen,
    folderRemove,
    folder,
    gift,
    globeAlt,
    globe,
    hand,
    hashtag,
    heart,
    home,
    identification,
    inboxIn,
    inbox,
    informationCircle,
    key,
    library,
    lightBulb,
    lightningBolt,
    link,
    locationMarker,
    lockClosed,
    lockOpen,
    login,
    logout,
    mailOpen,
    mail,
    map,
    menuAlt1,
    menuAlt2,
    menuAlt3,
    menuAlt4,
    menu,
    microphone,
    minusCircle,
    minusSm,
    minus,
    moon,
    musicNote,
    newspaper,
    officeBuilding,
    paperAirplane,
    paperClip,
    pause,
    pencilAlt,
    pencil,
    phoneIncoming,
    phoneMissedCall,
    phoneOutgoing,
    phone,
    photograph,
    play,
    plusCircle,
    plusSm,
    plus,
    presentationChartBar,
    presentationChartLine,
    printer,
    puzzle,
    qrcode,
    questionMarkCircle,
    receiptRefund,
    receiptTax,
    refresh,
    reply,
    rewind,
    rss,
    saveAs,
    save,
    scale,
    scissors,
    searchCircle,
    search,
    selector,
    server,
    share,
    shieldCheck,
    shieldExclamation,
    shoppingBag,
    shoppingCart,
    sortAscending,
    sortDescending,
    sparkles,
    speakerphone,
    star,
    statusOffline,
    statusOnline,
    stop,
    sun,
    support,
    switchHorizontal,
    switchVertical,
    table,
    tag,
    template,
    terminal,
    thumbDown,
    thumbUp,
    ticket,
    translate,
    trash,
    trendingDown,
    trendingUp,
    truck,
    upload,
    userAdd,
    userCircle,
    userGroup,
    userRemove,
    user,
    users,
    variable,
    videoCamera,
    viewBoards,
    viewGridAdd,
    viewGrid,
    viewList,
    volumeOff,
    volumeUp,
    wifi,
    xCircle,
    x,
    zoomIn,
    zoomOut
};
export { default as default } from './Icon.vue';
